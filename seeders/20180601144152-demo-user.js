'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Users', [{
			firstName: 'John',
			lastName: 'Doe',
			email: 'demo@demo.com',
            createdAt:'04/28/2015 21:13:09',
			updatedAt:'04/28/2015 21:13:09'
		}], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Users', null, {});
	}
};
