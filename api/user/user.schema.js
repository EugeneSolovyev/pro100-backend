const Sequelize = require('sequelize');

const SCHEMA = mongoose.Schema;

const NOTE_SCHEMA = new SCHEMA({
    title: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true
    }
}, {collection: 'notes'});

mongoose.model('notes', NOTE_SCHEMA);

module.exports = {
    notes: mongoose.model('notes'),
}