const path = require('path');

module.exports = {
    logger: require('./logger/logger.js').loggerService,
	config: require('../config/config')
};