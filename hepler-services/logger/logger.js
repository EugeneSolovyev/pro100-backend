const winston = require('winston');
const path = require('path');
const fs = require('fs');

const LOG_DIR = 'logs';

if (!fs.existsSync(LOG_DIR)) {
    fs.mkdirSync(LOG_DIR);
}

const loggerService = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ 
            filename: path.join(LOG_DIR, 'error.log'), 
            level: 'error'
         }),
        new winston.transports.File({ 
            filename: path.join(LOG_DIR, 'warning.log'),
             level: 'warning' 
            }),
        new winston.transports.File({ 
            filename: path.join(LOG_DIR, 'info.log'), 
            level: 'info' 
        }),
    ]
});

if (process.env.NODE_ENV !== 'production') {
    loggerService.add(new winston.transports.Console({
        format: winston.format.simple()
    }));
}

module.exports = {
    loggerService,
}